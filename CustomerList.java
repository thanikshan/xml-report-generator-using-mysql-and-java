//package Assignment5;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "customer_list")
public class CustomerList {
	
	@XmlElement(name = "customer")
	List<Customer> customerObjects = new ArrayList<>();

	
	public CustomerList() {
		super();
	}

	public CustomerList(List<Customer> customerObjects) {
		super();
		this.customerObjects = customerObjects;
	}

	public List<Customer> getCustomerObjects() {
		return customerObjects;
	}

	public void setCustomerObjects(List<Customer> customerObjects) {
		this.customerObjects = customerObjects;
	}
}
