//package Assignment5;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "product_list")
public class ProductList {
	@XmlElement(name = "product_set")
	private List<ProductSet> product_set = new ArrayList<>();

	public ProductList() {
		super();
	}

	public ProductList(List<ProductSet> product_set) {
		super();
		this.product_set = product_set;
	}

	public List<ProductSet> getProduct_set() {
		return product_set;
	}

	public void setProduct_set(List<ProductSet> product_set) {
		this.product_set = product_set;
	}
	
	public void addProduct_set(ProductSet productSet) {
		getProduct_set().add(productSet);
	}
	
	
}
