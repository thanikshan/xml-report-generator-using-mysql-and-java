//package Assignment5;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class XmlGenerator {

	//Database variables to create connection
	Connection connection = null;
	Statement statement = null;
	ResultSet resultSet = null;
	
	//Array list to store the objects of respective class
	List<Customer> customerObjects = new ArrayList<>();
	List<Product> productObjects = new ArrayList<>();
	List<Employee> employeeObjects = new ArrayList<>();
	
	private String startDate;
	private String endDate;
	private String path;
	
	
	// class objects:
	
	private CustomerList cList;
	private ProductList productList;
	private StaffList staffList;

	
	
	public XmlGenerator(String startDate, String endDate, String path) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.path = path;
	}

	//method to establish connection the database
	public int connectDB() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://db.cs.dal.ca:3306?serverTimezone=UTC", "tshanmugam",
					"B00798284");
			statement = connection.createStatement();
			statement.execute("use csci3901;");
			return 1;
		} catch (Exception e) {
			return 0;
		}

	}

	//method to execute the query.This method will accept arguments in the form of string
	public ResultSet queryExecutor(String query) throws SQLException {
		ResultSet temp = statement.executeQuery(query);
		return temp;
	}

	//method to generate the customer data for the given range
	public Boolean customerXml() {
		try {
			String query = "Select customers.customerName , customers.addressLine1, customers.city,customers.state, customers.postalCode, customers.country,count(orders.orderNumber) as numberOfOrders ,sum(orderdetails.priceEach * orderdetails.quantityOrdered) as totalOrderValues from orders inner join customers on orders.customerNumber = customers.customerNumber inner join orderdetails on orders.orderNumber = orderdetails.orderNumber where orders.orderdate between '"
					+ startDate + "' and '" + endDate + "' group by orders.customerNumber;";
			resultSet = queryExecutor(query);
			while (resultSet.next()) {
				Address address = new Address(resultSet.getString("addressLine1"), resultSet.getString("city"),
						resultSet.getString("postalCode"), resultSet.getString("country"));

				customerObjects.add(new Customer(resultSet.getString("customerName"), address,
						resultSet.getString("numberOfOrders"), resultSet.getString("totalOrderValues")));

			}
			cList = new CustomerList(customerObjects);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Issue with generating Customer details");
			return false;
		}
	}
	//method to generate the product data which stores the data in the productObjectList
	public Boolean productXml() {
		try {
			String query = "Select products.productLine, products.productName, products.productVendor , sum(orderdetails.quantityOrdered) as unitSold ,  sum(orderdetails.quantityOrdered * orderdetails.priceEach) as totalSales from products inner join orderdetails on products.productCode = orderdetails.productCode inner join orders on orderdetails.orderNumber = orders.orderNumber where orders.orderdate between '"
					+ startDate + "' and '" + endDate
					+ "' group by products.productCode order  by products.productLine,products.productName;";
			resultSet = queryExecutor(query);
			String prev = "";
			productList = new ProductList();
			int count = 0;
			while (resultSet.next()) {
				if (resultSet.getString("productLine").equals(prev) || prev.isEmpty()) {
					count++;
					prev = resultSet.getString("productLine");
					productObjects
							.add(new Product(resultSet.getString("productName"), resultSet.getString("productVendor"),
									resultSet.getString("unitSold"), resultSet.getString("totalSales")));
				} else {
					count++;
					ProductSet productset = new ProductSet(prev, productObjects);
					productList.addProduct_set(productset);
					prev = resultSet.getString("productLine");
					productObjects = new ArrayList<>();
					productObjects
							.add(new Product(resultSet.getString("productName"), resultSet.getString("productVendor"),
									resultSet.getString("unitSold"), resultSet.getString("totalSales")));
				}
			}
			if(!prev.isEmpty())
			{
			ProductSet productset = new ProductSet(prev, productObjects);
			productList.addProduct_set(productset);
			}
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Issue with generating product details");
			return false;
		}
	}
	//method to generate the employee related data for the given date range
	public Boolean employeeXml() {
		try {
			String query = "select employees.firstName,employees.lastName , offices.city,count(distinct(orders.customerNumber)) as activeCustomers , sum(orderdetails.quantityOrdered * orderdetails.priceEach) as totalSales from employees inner join offices on employees.officeCode = offices.officeCode inner join customers on employees.employeeNumber = customers.salesRepEmployeeNumber inner join orders on customers.customerNumber = orders.customerNumber inner join orderdetails  on orders.orderNumber = orderdetails.orderNumber where orders.orderdate between '"
					+ startDate + "' and '" + endDate + "' group by employees.employeeNumber;";
			resultSet = queryExecutor(query);
			while (resultSet.next()) {
				employeeObjects.add(new Employee(resultSet.getString("firstName"), resultSet.getString("lastName"),
						resultSet.getString("city"), resultSet.getString("activeCustomers"),
						resultSet.getString("totalSales")));
			}
			staffList = new StaffList(employeeObjects);
			return true;
		} catch (Exception e) {
			System.out.println("Issue with generating Employee details");
			return false;
		}
	}
	
	
//method which call the other specific  methods and pass the data to the root object and generates the xml
	public int generateXml() throws SQLException, JAXBException, IOException {
		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(YearEndSummary.class);
		int temp = connectDB();
		if (temp == 1) {
			customerXml();
			productXml();
			employeeXml();
			Year y = new Year(startDate, endDate);

			YearEndSummary yearendsummary = new YearEndSummary(y, cList, productList, staffList);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(yearendsummary, sw);
			jaxbMarshaller.marshal(yearendsummary, new FileWriter(path));
			connection.close();

		} else {
			System.out.println("Database Not Connected");
			return 0;
		}

		return 1;
	}

}
