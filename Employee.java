//package Assignment5;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "employee")


public class Employee {
	
	@XmlElement(name = "firstName")
	private String firstName;
	@XmlElement(name = "lastName")
	private String lastName;
	@XmlElement(name = "city")
	private String city;
	@XmlElement(name = "activeCustomers")
	private String activeCustomers;
	@XmlElement(name = "totalSales")
	private String totalSales;
	
	
	public Employee(String firstName, String lastName, String city, String activeCustomers, String totalSales) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.activeCustomers = activeCustomers;
		this.totalSales = totalSales;
	}


	public Employee() {
		super();
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getActiveCustomers() {
		return activeCustomers;
	}


	public void setActiveCustomers(String activeCustomers) {
		this.activeCustomers = activeCustomers;
	}


	public String getTotalSales() {
		return totalSales;
	}


	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}
	
	
	
	


}
