//package Assignment5;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "product")

public class Product {

	@XmlElement(name = "product_name")
	private String productName;
	@XmlElement(name = "product_vendor")
	private String productVendor;
	@XmlElement(name = "units_sold")
	private String unitSold;
	@XmlElement(name = "total_sales")
	private String totalSales;

	public Product() {
		super();
	}

	public Product(String productName, String productVendor, String unitSold, String totalSales) {
		super();
		this.productName = productName;
		this.productVendor = productVendor;
		this.unitSold = unitSold;
		this.totalSales = totalSales;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public String getUnitSold() {
		return unitSold;
	}

	public void setUnitSold(String unitSold) {
		this.unitSold = unitSold;
	}

	public String getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}

}
