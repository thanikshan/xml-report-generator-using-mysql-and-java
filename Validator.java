//package Assignment5;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Validator {
	private String dateFormat = "yyyy-MM-dd";

	//method to check whether the given date is in  valid format
	public int datevalidator(String date)
	{
		SimpleDateFormat simpledateformat = new SimpleDateFormat(dateFormat);
		simpledateformat.setLenient(false);
		try {
			Date dateToValidate = simpledateformat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return 0;
		}
	return 1;	
	}
	
	//method to check whether the path exist  or filename is valid
	public int filevalidator(File tempFile,String userPath)
	{
		int lastIndexOf;
		String absolutepath=tempFile.getAbsolutePath();
		if((absolutepath.length() == 0) || (absolutepath.equals("")))
		{
			System.out.println("invalid filename");
			return 0;
		}
		if(absolutepath.contains("\\"))
		{
			lastIndexOf = absolutepath.lastIndexOf("\\");
		}
		else
		{
			lastIndexOf = absolutepath.lastIndexOf("/");
		}
		if(lastIndexOf == -1)
		{
		return 0;	
		}
		String path = absolutepath.substring(0, lastIndexOf);
		String filename = absolutepath.substring(lastIndexOf+1, absolutepath.length());
		File f=new File(path);
		if( !(f.isDirectory())  || (filename.contains(".")) || (userPath.isEmpty()))
		{
			//System.out.println("Invalid directory");
			return 0;
		}
		return 1;
	}
	
	//method to compare the start date and end date
	public int dateComparison(String startDate,String  endDate)
	{
		 SimpleDateFormat sdformat = new SimpleDateFormat(dateFormat);		    
		Date d1 = null;
		try {
			d1 = sdformat.parse(startDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    Date d2 = null;
		try {
			d2 = sdformat.parse(endDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			
		}
	 if(d1.before(d2) || (startDate.equals(endDate))) 
	 {
		 return 1;
	 }
	 else
	 {
		 System.out.println("Start Date should be before the End date");
		 return 0;
	 }
	}
}
