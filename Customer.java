//package Assignment5;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "customer")
public class Customer {
	

	@XmlElement(name = "customer_name")
	private String customerName; 
	@XmlElement(name = "address")
	private Address address;
	@XmlElement(name = "num_orders")
	private String numberOfOrders;
	@XmlElement(name = "order_value")
	private String totalOrderValues;
	
	
	public Customer() {
		super();
	}
	public Customer(String customerName, Address address, String numberOfOrders, String totalOrderValues) {
		super();
		this.customerName = customerName;
		this.address = address;
		this.numberOfOrders = numberOfOrders;
		this.totalOrderValues = totalOrderValues;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getNumberOfOrders() {
		return numberOfOrders;
	}
	public void setNumberOfOrders(String numberOfOrders) {
		this.numberOfOrders = numberOfOrders;
	}
	public String getTotalOrderValues() {
		return totalOrderValues;
	}
	public void setTotalOrderValues(String totalOrderValues) {
		this.totalOrderValues = totalOrderValues;
	}

	

	
	
	

}
