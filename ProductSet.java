//package Assignment5;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "product_set")


public class ProductSet {
	
	@XmlElement(name = "product_line_name")
	String productLineName;
	@XmlElement(name = "product")
	List<Product> productObjects = new ArrayList<>();

	
	
	public ProductSet() {
		super();
	}



	public ProductSet(String productLineName, List<Product> productObjects) {
		super();
		this.productLineName = productLineName;
		this.productObjects = productObjects;
	}



	public String getProductLineName() {
		return productLineName;
	}



	public void setProductLineName(String productLineName) {
		this.productLineName = productLineName;
	}



	public List<Product> getProductObjects() {
		return productObjects;
	}



	public void setProductObjects(List<Product> productObjects) {
		this.productObjects = productObjects;
	}

	 
}
