//package Assignment5;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "year_end_summary")
public class YearEndSummary {

	@XmlElement(name = "year", required = true)
	private Year year = new Year();
	@XmlElement(name = "customer_list", required = true)
	private CustomerList customerlist = new CustomerList();
	@XmlElement(name = "product_list", required = true)
	private  ProductList productList=new ProductList();
	@XmlElement(name = "staff_list", required = true)	
	private StaffList staffList = new StaffList();
	
	


	public YearEndSummary(Year year, CustomerList customerlist, ProductList productList, StaffList staffList) {
		super();
		this.year = year;
		this.customerlist = customerlist;
		this.productList = productList;
		this.staffList = staffList;
	}




	public YearEndSummary() {
		super();
	}

	
}
