//package Assignment5;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "staff_list")

public class StaffList {

	@XmlElement(name = "employee")
	List<Employee> employeeObjects = new ArrayList<>();

	public StaffList(List<Employee> employeeObjects) {
		super();
		this.employeeObjects = employeeObjects;
	}

	public StaffList() {
		super();
	}
	
	
}
