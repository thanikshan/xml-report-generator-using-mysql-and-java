//package Assignment5;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Year")
public class Year {

	
	@XmlElement(name = "start_date", required = true)
	private String startDate;
	@XmlElement(name = "end_date", required = true)
	private String endDate;
	
	
	public Year(String startDate, String endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}


	public Year() {
		super();
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
}
