//package Assignment5;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.xml.bind.JAXBException;

public class XmlGeneratorUI {

	public static void main(String[] args) throws SQLException, IOException {

		String startDate;
		String endDate;
		String filename;
		String path;
		int count = 1;
		int checker;
		int dateCheck;
		File f;
		
		//do while loop to check whether the start date is smaller than the end  date
		do {
			Scanner inScanner = new Scanner(System.in);
			Validator v = new Validator();
			do
			{
				System.out.println("Enter Year Start Date in YYYY-MM-DD for summary generation");
				
			do {
				if (count == 0) {
					System.out.println("Incorrect date format please re-enter");
					System.out.println("Enter Year Start Date in YYYY-MM-DD for summary generation");
				}
				startDate = inScanner.nextLine();
				count = v.datevalidator(startDate);

			} while (count == 0);

			count = 1;
			System.out.println("Enter Year End Date in YYYY-MM-DD for summary generation");
			do {
				if (count == 0) {
					System.out.println("Incorrect date format please re-enter");
					System.out.println("Enter year End Date in YYYY-MM-DD for summary generation");
				}
				endDate = inScanner.nextLine();
				count = v.datevalidator(endDate);

			} while (count == 0);
			dateCheck = v.dateComparison(startDate, endDate);
			}while(dateCheck == 0);
			
			
			System.out.println("Enter the file name or path and filename without extension");
			do {
				if (count == 0) {
					System.out.println("Incorrect file name or path format please re-enter");
					System.out.println("Enter the file name or path without extension");

				}
				filename = inScanner.nextLine();
				f = new File(filename);
				count = v.filevalidator(f,filename);

			} while (count == 0);
			path = filename + ".xml";
			XmlGenerator x = new XmlGenerator(startDate, endDate, path);
			try {
				int i=x.generateXml();
				if(i == 1)
				{
				System.out.println("XML generated successfully");
				System.out.println("---------------------------");
				System.out.println("Path Written:" +f.getAbsolutePath());
				System.out.println("---------------------------");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Unable to generate data");
			}
			
			System.out.println("Press 1:  To generate new set of data \nPress any other number to Quit");
			checker=inScanner.nextInt();
		} while (checker == 1);
	}
}
